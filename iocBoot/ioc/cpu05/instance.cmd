###############################################################################
#- Macro line should start with '### MACRO' folowed by macro name
#- and default value:
#-   ### MACRO <MACRO NAME> <MACRO DEFAULT VALUE>
#-
#- All the lines (except ### MACRO lines) will be copied verbatim to final
#- st.cmd file in the instance folder.
#-
#- Following macros need to be defined here as they are expected in the
#- common.cmd.
epicsEnvSet("CONTROL_GROUP", "LAB-FC05")
epicsEnvSet("AMC_NAME", "Ctrl-AMC-003")
epicsEnvSet("AMC_DEVICE", "/dev/sis8300-3")
epicsEnvSet("EVR_NAME", "LAB-FC05:TS-EVR-000:")
epicsEnvSet("SYSTEM1_PREFIX", "LAB-010:PBI-FC-001:")
epicsEnvSet("SYSTEM1_NAME", "Lab FC 1")
epicsEnvSet("SYSTEM1_CHANNEL", "0")
epicsEnvSet("SYSTEM2_PREFIX", "LAB-010:PBI-FC-002:")
epicsEnvSet("SYSTEM2_NAME", "Lab FC 2")
epicsEnvSet("SYSTEM2_CHANNEL", "1")
epicsEnvSet("SYSTEM3_PREFIX", "LAB-010:PBI-FC-003:")
epicsEnvSet("SYSTEM3_NAME", "Lab FC 3")
epicsEnvSet("SYSTEM3_CHANNEL", "2")
epicsEnvSet("SYSTEM4_PREFIX", "LAB-010:PBI-FC-004:")
epicsEnvSet("SYSTEM4_NAME", "Lab FC 4")
epicsEnvSet("SYSTEM4_CHANNEL", "3")
epicsEnvSet("SYSTEM5_PREFIX", "LAB-010:PBI-FC-005:")
epicsEnvSet("SYSTEM5_NAME", "Lab FC 5")
epicsEnvSet("SYSTEM5_CHANNEL", "4")

###############################################################################
