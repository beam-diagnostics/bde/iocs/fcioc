#!/bin/bash
#
# script to automate collection of ADC channel noise into TIFF file
# using fcioc and its AD plugins.
#
. $HOME/bde/tools/src/dev.env

# export EPICS_CA_ADDR_LIST="bicpu2.lab.esss.lu.se"
# only work with local IOC
export EPICS_CA_ADDR_LIST="localhost"
export EPICS_CA_AUTO_ADDR_LIST="NO"

P="LAB-010:PBI-FC-001:AQ3"
[[ $# -gt 0 ]] && P="$1"
[[ -n $P ]] || { echo "Usage: $0 PV_PREFIX"; exit 1; }

#caget -h
#caput -h

function pvget {
  echo -n "$1: "
  caget -t $P-$2
}

function pvgets {
  echo -n "$1: "
  caget -t -S $P-$2
}

function pvput {
  echo -n "$1: "
  caput -t $P-$2 "$3"
}

pvgets "Device type" DeviceTypeStrR
pvgets "Device firmware" FWVersionR
pvgets "Device serial nr." SerialNumberR

pvput "Trigger repeat" TrigRepeat 1
pvput "Channel status" 1-Control Enabled
pvput "PROC plugin data type" 1-PR1-DataTypeOut Automatic

# XXX should probably set the path to a shared drive or something
#     for now leave at what startup/user has already set
# dbpf $(ACQ_PREFIX)1-TF1-FilePath "$(PWD)"
# dbpf $(ACQ_PREFIX)1-TF1-FileName "channel1_bg"
# dbpf $(ACQ_PREFIX)1-TF1-FileTemplate "%s%s.tiff"
pvgets "TIFF plugin file path" 1-TF1-FilePath_RBV
pvgets "TIFF plugin file name" 1-TF1-FileName_RBV
pvgets "TIFF plugin file template" 1-TF1-FileTemplate_RBV
pvgets "TIFF plugin last file" 1-TF1-FullFileName_RBV
pvput "TIFF plugin autosave" 1-TF1-AutoSave Yes
pvput "TIFF plugin status" 1-TF1-EnableCallbacks Enable

pvget "TIFF plugin last id" 1-TF1-UniqueId_RBV
pvput "Channel state" Acquire Acquire

pvget "TIFF plugin last id" 1-TF1-UniqueId_RBV
pvgets "TIFF plugin last file" 1-TF1-FullFileName_RBV
pvput "TIFF plugin status" 1-TF1-EnableCallbacks Disable
pvput "Channel status" 1-Control Disabled
tiffinfo $(caget -t -S $P-1-TF1-FullFileName_RBV)
