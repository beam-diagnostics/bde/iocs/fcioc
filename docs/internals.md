# IOC internals

This document tries to describe the IOC internals including, but not limited to,
AD driver, individual processing chains, IOC design and startup.

## Design

IOC is designed around AD driver (source of `NDArray`s) and chains of AD plugins; processing chains. Driver provides analog channel input data as a NDArray object, for each input channel separately. NDArray object hold channel sample data along with other meta data that AD provides.

AD driver is considered a source of data for plugins to work on. In addition to data it also controls the acquisition of the sample data. User is allowed to set trigger source, clock source (sampling rate), amount of samples, signal attenuation, to mention few of the available controls. Each analog input channel is controlled individually, and can be enabled or disabled.

Processing chain is built with chaining AD plugins that work on received sample data. Plugins usually act as consumers and producers of NDArray objects. There are some plugins that act as consumers only, i.e. NDStdArray, ADPluginRedis. A data channel has multiple processing chains, defined in IOC startup files. Defined processing chains can be easily extended or new processing chains added (AD is built with full support of all known AD plugins).

Autosave features are used for many AD core, driver and plugin PVs. This means that after restart, IOC will be set up with last known values from the previous run.

Non-EPICS utility `procServ` is used for IOC startup in daemon mode, allowing user to reach IOC shell over telnet.

## Supporting modules

IOC depends on several EPICS and non-EPICS support packages.

	epicsEnvSet("SSCAN","/opt/bde/R3.15.5/sscan-R2-11-1")
	epicsEnvSet("PROCSERV","/opt/bde/R3.15.5/procserv-V2.7.0")
	epicsEnvSet("ASYN","/opt/bde/R3.15.5/asyn-R4-33")
	epicsEnvSet("AUTOSAVE","/opt/bde/R3.15.5/autosave-R5-9")
	epicsEnvSet("ECAT2","/opt/bde/R3.15.5/ecat2-v2.1.2")
	epicsEnvSet("DEVLIB2","/opt/bde/R3.15.5/devlib2-2.9")
	epicsEnvSet("ETHERCAT","/opt/bde/R3.15.5/ethercat-R1-5-99")
	epicsEnvSet("ADSUPPORT","/opt/bde/R3.15.5/adsupport-R1-4")
	epicsEnvSet("BUSY","/opt/bde/R3.15.5/busy-R1-6-1")
	epicsEnvSet("ECAT2DB","/opt/bde/R3.15.5/ecat2db-v0.4.3")
	epicsEnvSet("MRFIOC2","/opt/bde/R3.15.5/mrfioc2-2.2.0-ess-rc3")
	epicsEnvSet("CALC","/opt/bde/R3.15.5/calc-R3-7")
	epicsEnvSet("ADCORE","/opt/bde/R3.15.5/adcore-R3-3-2")
	epicsEnvSet("ADMISC","/opt/bde/R3.15.5/admisc-R2-0")
	epicsEnvSet("ADSIS8300","/opt/bde/R3.15.5/adsis8300-R1-3")
	epicsEnvSet("ADSIS8300FC","/opt/bde/R3.15.5/adsis8300fc-R1-3")
	epicsEnvSet("EPICS_BASE","/opt/bde/R3.15.5/epics-base-R3.15.5")

## AreaDetector NDArray Driver

The AD driver for IOC extends `asynNDArrayDriver`. It is responsible for controlling the acquisition and delivering channel sample data. Channel sample data is updated on trigger rate, sampled at the user specified sampling frequency. AD driver provides only 'raw' analog input sample data; this is the data produced by the FPGA firmware by driving ADCs. The sample data may be processed by the firmware, nevertheless it can still be considered as 'raw' at this point as far as software is concerned.

Any sample data processing bound to software, done in IOC, is reserved for AD plugins.

## Processing chains

IOC employs three main processing chains:

* __pulse__ chain
* __flat top__ chain
* debug chain

Pulse and flat top chains are meant for LCR, operator use. Debug chain is for engineering purposes and would usually not be used by the operators but PBI engineers. This document shall focus on the LCR processing chains, and mention debug chain in brief.

### Common pre-processing

Pulse and flat top chains both start with common pre-processing where background is subtracted and sample data converted to engineering units. For this purpose `NDPluginProcess` is used. Plugin consumes the 'raw' sample data and produces converted sample data for other plugins (chains).

By default background subtraction is disabled, but can be enabled on used demand. It must be configured first, though. This is achieved by storing a snapshot of the 'raw' sample data when no analog input is present (background); we need to collect analog input noise. Afterwards, when the background subtraction is enabled, each incoming 'raw' sample data waveform shall be subtracted by the saved background sample data waveform, sample by sample. With this approach we effectively bring the baseline of the analog input to 0.

By default data type conversion is disabled, but can be enabled on user demand. User can choose the output data type to be: signed integer, unsigned integer, floating point. This is useful when converting raw ADC sample value to engineering units such as volt or amperes.

By default sample clipping is disabled, but can be enabled on user demand. User must specify the cutoff point a which we would like to clip the incoming sample data to.

By default pre-processing feeds the pulse and flat top chains by default, the processing performed here shall have affect on the subsequent plugins' source data.

### Pulse chain

Pulse chain consumes the pre-processed data and delivers __complete pulse__ waveforms to EPICS CA clients. The chain allows user to specify decimation, ROI and pulse-by-pulse filtering (averaging). This chain is meant for LCR monitoring, and can be configured to minimize the load on the IOC, network and CA clients.

By default all samples received from the pre-processing shall be processed by this chain. Fine tuned sample data start and size is controlled by the ROI parameters. It can be used to adjust the window of the sample data if it contains too many head or tail samples.

By default sample data resolution is as received from pre-processing (no decimation). This means that sampling at more than 10e8 samples per second yields large amount of data per pulse. For this purpose the sample data decimation can be enabled. This can be effectively used to lower the number of data samples per pulse.

By default CA client shall receive pulse waveform updates at the trigger rate. This means that trigger rate at more than 5x per second generates unnecessary PV updates for slow LCR monitoring. For this purpose pulse waveforms can be averaged. This can be effectively used to throttle the rate of pulse waveforms received by the CA client.

### Flat top chain

Flat top chain consumes the pre-processed data and delivers __pulse flat top__ waveforms to EPICS CA clients. The chain allows user to specify decimation, ROI and pulse-by-pulse filtering (averaging). This chain is meant for LCR monitoring, and should be configured to capture the interesting part of the pulse, the flat top, in order to calculate the charge of the pulse (area under the flat top).

The flat top chain has lots common with pulse chain when it comes to user controls. Essentially it allows the same processing to take place, while the source of the data is focused around the flat top (while the pulse chain would hold head and tail samples, too).

This chain also provides flat top sample data statistics including, but not limited to, minimum, maximum, mean and standard deviation for each pulse; as scalar value and time series waveform.

## User interface

CSS based BOY engineering screens are provided with the IOC. Many come from the supporting modules (ADCore, digitizer AD driver, autosave, asyn,..). IOC specific OPI include module OPI and provide over-arching control of all the system units.

AD driver OPI was built to provide complete control of the digitizer and production of 'raw' sample data.

AD plugin OPI are adopted to control the instances of AD plugins used in every processing chain.

JS is used to facilitate the updates of the certain widgets elements.

As a visualization aid. user is provided with 'cursors' that show start and end of the flat top area on the complete pulse plot.

## References

* [EPICS](https://epics.anl.gov) home page
* [areaDetector](http://cars9.uchicago.edu/software/epics/areaDetector.html) home page
* [BDE Gitlab](https://gitlab.esss.lu.se/beam-diagnostics/bde) pages


## Acronyms

| acronym | decription |
|-|-|
|AD|areaDetector|
|ADC|Analog to Digital Converter|
|BDE|Beam Diagnostics EPICS environment|
|BOY|Best OPI Yet? (EPICS)|
|CA|Channel Access (EPICS)|
|CSS|Control System Studio(EPICS)|
|EPICS|Experimental Physics and Industrial Control System|
|IOC|Input / Output Controller (EPICS)|
|JS|JavaScript|
|LCR|Local Control Room|
|PBI|Proton Beam Instrumentation|
|PV|Process Variable (EPICS)|
|ROI|Region Of Interest|
