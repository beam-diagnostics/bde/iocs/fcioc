# Faraday Cup Operation

Current readout is performed using SIS8300-KU AMC and SIS8900 RTM.
Single channel is used.
A resistor on the analog input is used to convert detected current to voltage.
Voltage is sampled at high rate (> Msps).

## Overview

![alt plugin chain layout](chain_overview.jpg)


## Generic acquisition

Device information:
 
 * AMC device type is SIS8300-KU, 2 GB DDR4 memory
 * RTM device type is SIS8900, HiZ, +/- 10 V input range, 16 ADC
 * Maximum sampling frequency is 125 MHz
 * volts per ADC = 20 V / 65536 = 0.000305176 V (305.176 uV)
 * RTM does not support attenuation
 * Software needs to poll for data acquisition end condition

Following parameters need to be setup:

 * Sampling frequency source
     * internal, external
 * Sampling frequency divider
     * &gt;2 if using internal source, 1 if external
 * Sampling frequency value (only if external sampling frequency source is used)
     * used to derive sample delta time
     * needs to be manually set by the user in case of external sampling frequency source
 * Trigger source
     * use EVR generated trigger on backplane line 0
 * Trigger repeat
     * set to -1 for infinite
 * Enable channel 1
 * Number of samples
     * maximum value is 100000
     * sample has delta time of 1 / sampling frequency value
 * RTM type is SIS8900

At this point a signal from firmware, for the channel 1, shall be available for processing by the IOC.
Debug chain can be utilized at this point.

## Preprocessing chain

Preprocessing chain is responsible for background subtraction and sample data unit conversion.

For background subtraction to work, a snapshot of the channel ADC noise needs to be made, without applying input signal. The process plugin is capable of loading the acquired noise sample ahead of the normal operation. During normal operation the noise is subtracted from the each waveform processed. 

### Manual setup

Attach the Trace 1 to Proc 1 output (asyn port CH0.PROC1).
Disable all the Proc 1 features.
Note the signal on Trace 1, samples are in ADC counts
Trace 1 offset should be above 32768 ADC values.
Set scale to 1 and offset to -32768 in Proc 1
Enable the Scale and offset block in Proc 1
Trace 1 offset should be near 0 in ADC values.
ADC electrical noise offset depends on each ADC chip and PCB; it is expected to be not 0
Correct the by offset manually adjusting the Proc 1 offset parameter.
Trace 1 offset should be around 0 in ADC values.

### Setup with noise ADC snapshot

ADC noise snapshot is created as follows:

* Set the acquisition parameters as desired (sampling frequency, number of samples,..)
* Disable sample data unit conversion in PR1
* Set acquisition repeat to 1
* Enable TIFF file saving plugin
* Setup TIFF file saving parameters (file path, file name, file name format)
* Set auto save to Yes
* Perform the acquisition (can be repeated several times if desired)
* The TIFF file will be saved on each acquisition
* Verify that the file has been saved
* Repeat if desired
* Disable TIFF file saving plugin

For unit conversion to work properly ADC counts need to be converted to amperes (current). ADC on RTM sees input voltage change (voltage drop on the resistor) when input current changes. A conversion factor from ADC counts to amperes needs to be setup:

    1 ADC count == 305 uV
    input range +/- 10 V (20 V)
    maximum current 100 mA
    R = U / I

The pre-processing parameter setup is as follows:

 * Enable pre-processing (PR1)
 * Setup background subtraction TIFF read (file path, file name, file name format)
   to point the file acquired, see above)
 * Enable background subtraction (TIFF with ADC noise as background needs to be available before enabling this feature, see above)
 * Set scaling parameter that results in ADC count to amperes conversion
 * Set offset parameter to 0 (offset is removed with background subtraction)
 * Set output data type to Float64
 * Do not enable recursive filter, clipping and flat field normalization

## Complete pulse

Complete pulse chain is responsible for delivering the complete pulse waveform including the pulse head, flat top and tail. User can adjust number of samples in head and tail, sample decimation factor (within a single pulse), pulse averaging factor (over several consecutive pulses).

The complete pulse chain parameter setup is as follows:

* Enable RI2 plugin (region of interest)
* Set binning in X direction to desired value
   * will decimate the number if data samples within a pulse waveform resulting in fewer samples per pulse waveform
* Enable scaling in the RI2 plugin
   * set scaling divisor to same value as binning in X direction in order to maintain the proper sample data range
* Adjust the ROI start and size as desired
   * optional, can be used to adjust the start and end of the pulse
* Set the region name (to i.e. Pulse)
* Do not adjust Y and Z dimension parameters

* Enable PR2 plugin (processing)
* Enable recursive filter to perform averaging over several pulses
   * set N filter to number of pulses to average (N=14; 1 per second PV update @ 14 Hz machine rate)
   * select Average filter type
   * set auto reset filter to yes
   * generate array callback for Array N only
   * leave the OCx, FCx and RCx at their defaults
* Do not enable background subtraction, flat field normalization, scaling and offset, clipping and output data type

* Enable TR2 plugin (monitor)
   * will deliver waveforms to CA clients
   * size and rate depend on the RI2 and PR2 plugin settings


## Pulse flat top

Pulse flat top chain is responsible for delivering the pulse flat top waveform and its statistics (minimum, maximum, mean, sigma,..). Similar controls as for complete pulse waveform are available (region of interest, processing and monitor). Statistics are reported as scalar values and time series waveforms of statistical values are also provided on this chain.

The complete pulse chain parameter setup is as follows:

* Enable RI3 plugin (region of interest)
* Set binning in X direction to desired value
   * will decimate the number if data samples within a pulse waveform resulting in fewer samples per pulse waveform
* Enable scaling in the RI3 plugin
   * set scaling divisor to same value as binning in X direction in order to maintain the proper sample data range
* Adjust the ROI start and size as desired
   * set start to cut off any head samples
   * set size/end to cut off any tail samples
* Set the region name (to i.e. Flat top)
* Do not adjust Y and Z dimension parameters

* Enable PR3 plugin (processing)
* Enable recursive filter to perform averaging over several pulses
   * set N filter to number of pulses to average (N=14; 1 per second PV update @ 14 Hz machine rate)
   * select Average filter type
   * set auto reset filter to yes
   * generate array callback for Array N only
   * leave the OCx, FCx and RCx at their defaults
* Do not enable background subtraction, flat field normalization, scaling and offset, clipping and output data type

* Enable TR3 plugin (monitor)
   * will deliver waveforms to CA clients
   * size and rate depend on the RI3 and PR3 plugin settings

* Enable ST3 plugin (statistics)
   * enable compute statistics part of the plugin
   * leave the rest disabled

* Enable ST3-TS plugin (statistics time series)
   * set number of points to hold in waveform to desired value
   * set leave averaging time to 0 (no averaging)
   * set acquisition mode to Circ. buffer
   * time link should point to RI3 sample time delta
   * start the collection of time series samples

# Issues

1. The pulse plot shows the trace start in the negative value (on X axis). This could be CS studio issue.

2. The PR1 unit conversion needs to be disabled when trying to acquire channel ADC noise into TIFF (for background subtraction). 
