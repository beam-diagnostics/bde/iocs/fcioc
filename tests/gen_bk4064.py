#!/bin/python3.5
#
# Control the pulse generation of:
# Bus 003 Device 009: ID f4ed:ee3a Shenzhen Siglent Co., Ltd. SDG1010 Waveform Generator (TMC mode)
#
# Actually BK PRECISION 4064:
# https://bkpmedia.s3.amazonaws.com/downloads/manuals/en-us/4060_series_manual.pdf
# https://bkpmedia.s3.amazonaws.com/downloads/datasheets/en-us/4060_series_datasheet.pdf
# https://bkpmedia.s3.amazonaws.com/downloads/programming_manuals/en-us/4060_series_programming_manual.pdf

import time
import usbtmc
instr = usbtmc.Instrument(0xf4ed, 0xee3a)
instr.ask("*IDN?")
instr.ask("*IDN?")
print(instr.ask("*IDN?"))


def rst():
    instr.write("*RST")


def nopulse():
    instr.write("C1:OUTP OFF")


def pulse():
    # setup nominal pulse
    instr.write("C1:OUTP OFF")
    instr.write("C1:BSWV WVTP,PULSE")
    instr.write("C1:BSWV AMP,8V")
    instr.write("C1:BSWV DUTY,4%")
    instr.write("C1:BSWV FRQ,14HZ")
    instr.write("C1:BSWV OFST,4V")
    # sync with external trigger (burst mode)
    instr.write("C1:BTWV STATE,ON")
    instr.write("C1:BTWV TRSR,EXT")
    instr.write("C1:OUTP ON")


def stat():
    # get the status
    instr.ask("C1:BSWV?")
    print(instr.ask("C1:BSWV?"))
    instr.ask("C1:BTWV?")
    print(instr.ask("C1:BTWV?"))
    instr.ask("C1:OUTP?")
    print(instr.ask("C1:OUTP?"))


rst()
time.sleep(3)

nopulse()
stat()

# turn off output for ADC noise collection
print("User can collect ADC noise with TIFF plugin.. press any key when done!")
ans = input()

pulse()
stat()

"""
$ gen_bk4064.py
*IDN *,4064,447F18102,5.01.01.12R3,05-00-00-17-36
C1:BSWV WVTP,PULSE,FRQ,14HZ,PERI,0.0714286S,AMP,8V,OFST,4V,HLEV,8V,LLEV,0V,DUTY,4,RISE,6e-09S,FALL,6e-09S,DLY,0
C1:BTWV STATE,ON,TRSR,EXT,TIME,1,DLAY,4.76965e-06S,EDGE,RISE,GATE_NCYC,NCYC,CARR,WVTP,PULSE,FRQ,14HZ,AMP,8V,OFST,4V,DUTY,4,RISE,6e-09S,FALL,6e-09S,DLY,0
C1:OUTP OFF,LOAD,HZ,PLRT,NOR
User can collect ADC noise with TIFF plugin.. press any key when done!

C1:BSWV WVTP,PULSE,FRQ,14HZ,PERI,0.0714286S,AMP,8V,OFST,4V,HLEV,8V,LLEV,0V,DUTY,4,RISE,6e-09S,FALL,6e-09S,DLY,0
C1:BTWV STATE,ON,TRSR,EXT,TIME,1,DLAY,4.76965e-06S,EDGE,RISE,GATE_NCYC,NCYC,CARR,WVTP,PULSE,FRQ,14HZ,AMP,8V,OFST,4V,DUTY,4,RISE,6e-09S,FALL,6e-09S,DLY,0
C1:OUTP ON,LOAD,HZ,PLRT,NOR
"""
